from js import document
from pyodide.ffi import to_js
import coscine


def _create_client():
    token = document.getElementById("token").value
    base_url = document.getElementById("baseURL").value.strip()

    if base_url:
        return coscine.ApiClient(token, base_url=base_url)
    return coscine.ApiClient(token)


errors_div = document.getElementById("errors")


def _reset_errors():
    errors_div.innerHTML = ""


def _append_resources_to_project(project, project_item):
    for resource in project.resources():
        radio_button = document.createElement("input")
        radio_button.id = resource.id
        radio_button.type = "radio"
        radio_button.name = "resource"  # name of the radio group
        radio_button.dataset.resourceid = resource.id
        radio_button.dataset.projectid = project.id

        label = document.createElement("label")
        label.htmlFor = radio_button.id
        label.innerText = resource.name

        div = document.createElement("div")
        div.appendChild(radio_button)
        div.appendChild(label)
        project_item.appendChild(div)


def _projects_bullet_list(projects):
    bullet_list = document.createElement("ul")
    for project in projects:
        project_item = document.createElement("li")
        project_item.innerText = project.name

        _append_resources_to_project(project, project_item)

        bullet_list.appendChild(project_item)
    return bullet_list


def show_projects():
    projects_div = document.getElementById("projects")
    projects_div.innerHTML = ""
    _reset_errors()

    try:
        client = _create_client()
        bullet_list = _projects_bullet_list(client.projects(toplevel=False))
    except Exception as e:
        errors_div.innerText = str(e)
        return
    projects_div.appendChild(bullet_list)


def _files_bullet_list(project, resource):
    bullet_list = document.createElement("ul")
    for file in resource.files(recursive=True):
        file_item = document.createElement("li")

        if file.is_folder:
            inner_text = "Folder: "
        else:
            inner_text = "File: "
            file_item.setAttribute("onclick", "downloadFile(event)")
        file_item.innerText = inner_text + file.path

        file_item.dataset.projectid = project.id
        file_item.dataset.resourceid = resource.id
        file_item.dataset.filepath = file.path

        bullet_list.appendChild(file_item)
    return bullet_list


def _selected_resource():
    return document.getElementById("projects").querySelector("input[name=resource]:checked")


def show_files():
    files_div = document.getElementById("files")
    files_div.innerHTML = ""
    _reset_errors()

    if not (checked_radio_button := _selected_resource()):
        return

    project_id = checked_radio_button.dataset.projectid
    resource_id = checked_radio_button.dataset.resourceid

    try:
        client = _create_client()
        project = client.project(project_id, coscine.Project.id)
        resource = project.resource(resource_id, coscine.Resource.id)

        bullet_list = _files_bullet_list(project, resource)
    except Exception as e:
        errors_div.innerText = str(e)
        return

    help_div = document.createElement("div")
    help_div.innerText = "(Click on a file to download)"

    files_div.appendChild(help_div)
    files_div.appendChild(bullet_list)


def download_file(target_element):
    _reset_errors()

    project_id = target_element.dataset.projectid
    resource_id = target_element.dataset.resourceid
    file_path = target_element.dataset.filepath

    try:
        client = _create_client()
        project = client.project(project_id, coscine.Project.id)
        resource = project.resource(resource_id, coscine.Resource.id)
        file = resource.file(file_path)

        # Highly inefficient because the file goes to Emscripten's in-memory file system first.
        # It is probably going to break for large files.
        file.download("/coscine_download")
        return to_js(file.path)
    except Exception as e:
        errors_div.innerText = str(e)
        return


def upload_file(file_name):
    _reset_errors()

    if not (checked_radio_button := _selected_resource()):
        return

    project_id = checked_radio_button.dataset.projectid
    resource_id = checked_radio_button.dataset.resourceid

    try:
        client = _create_client()
        project = client.project(project_id, coscine.Project.id)
        resource = project.resource(resource_id, coscine.Resource.id)

        metadata = resource.metadata_form()
        metadata["Title"] = file_name
        metadata["Creator"] = "{ME}"

        with open("/coscine_upload", "rb") as fp:
            resource.upload(file_name, fp, metadata)
    except Exception as e:
        errors_div.innerText = str(e)
        return

    show_files()
