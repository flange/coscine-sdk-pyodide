"use strict";

async function startPyodide() {
  const pyodide = await loadPyodide();

  await pyodide.loadPackage("micropip");
  const micropip = pyodide.pyimport("micropip");
  await micropip.install(["ssl", "sqlite3", "urllib3==2.0.7", "coscine"]);

  await loadPyModule("module.py", pyodide)

  console.log("Pyodide is ready!");
  return pyodide;
}

const pyodidePromise = startPyodide();

async function loadPyModule(name, pyodide) {
  const response = await fetch(name);
  const code = await response.text();
  return pyodide.runPythonAsync(code);
}

async function getPythonFunction(name) {
  return (await pyodidePromise).runPython(name);
}

async function showProjects() {
  const show_projects = await getPythonFunction("show_projects");
  show_projects();
}

async function showFiles() {
  const show_files = await getPythonFunction("show_files");
  show_files();
}

async function downloadFile(event) {
  const download_file = await getPythonFunction("download_file");
  const filename = download_file(event.target);

  // https://stackoverflow.com/a/54468787
  const content = (await pyodidePromise).FS.readFile("/coscine_download");
  const a = document.createElement('a');
  a.download = filename;
  a.href = URL.createObjectURL(new Blob([content], { type: "application/octet-stream" }));
  a.style.display = 'none';
  document.body.appendChild(a);
  a.click();
  URL.revokeObjectURL(a.href);
  document.body.removeChild(a);
}

async function uploadFile() {
  const file = document.getElementById("fileupload").files[0];
  if (!file) return;

  const reader = new FileReader();
  reader.fileName = file.name;
  reader.onload = async (event) => {
    const reader = event.target;
    const data = new Uint8Array(reader.result);
    (await pyodidePromise).FS.writeFile("/coscine_upload", data);

    const upload_file = await getPythonFunction("upload_file");
    upload_file(reader.fileName);
  }
  reader.readAsArrayBuffer(file);
}
